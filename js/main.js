/*Console.log a colores :)*/
var f=["background: linear-gradient(135deg, rgba(248, 97, 97, 1) 0%,rgba(105, 81, 255, 1) 100%);","color: white","padding: 10px 20px","line-height: 35px"].join(";");
console.log("%cMade by Pamstelito&MaryOcean <3",f);

/*canvas*/
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");

var center = { 
	x: 300,
	y: 200
};

ctx.beginPath();
ctx.moveTo(225,200);
ctx.lineTo(center.x,center.y);
ctx.strokeStyle = "#FF0000";
ctx.stroke();

for (i=0; i<400; i++) {
 	var x = Math.sin(i * (Math.PI/180)) * 100;
	var y = i + 200;
}

/*
P0=(225,200)
P1=(x,y)
P2=(300,100)
*/