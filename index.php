<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Estudio</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>	
	<header id="header">
		<img src="images/icono-lowe-desarrollo.jpg" width="100px">
	</header>
	<section>
		<?php $frase='yo no se si es privada esta frase'; $frasedos='yo no se si es publica esta frase'; ?>
		<h1>ESTUDIO</h1>
		<p>Aquí vamos a estudiar :)</p>
		<h2>Tarea</h2>
		<p>estudiar   <a href="https://www.markdowntutorial.com/"> Markdown </a></p>
		<p class="<?php if (strpos($frase, 'privada') !== false): ?> existe <?php endif;?>">frase primera</p>
		<p class="<?php if (strpos($frasedos, 'privada') !== false): ?> existe <?php endif;?>">frase segunda</p>
		<div class="m-canvas">
			<canvas id="myCanvas" width="600" height="400"></canvas>
			<button class="button button-black">kjh</button>
			<button class="button button-red">kjh</button>
		</div>
	</section>
	<footer>
		<script src="js/main.js"></script>
	</footer>
</body>
</html>